// QLChiTieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QLChiTieu.h"
#include <Windows.h>
#include <CommCtrl.h>
#include <fstream>
#include <locale>
#include <codecvt>
#include <vector>
using namespace std;
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100

// Các loại chi tiêu
#define AnUong 0
#define DiChuyen 1
#define NhaCua 2
#define XeCo 3
#define NhuYeuPham 4
#define DichVu 5

int draw = -1;
float percentOfType[6] = { 0 };
float moneyOftype[6] = { 0 };

HRGN saveShapeSpend[6];
// Các handle
HWND hListSpend, hContent, hAmountofmoney;
HWND hListView, hTotalMoney;
HWND hAnUong, hDiChuyen, hNhaCua, hXeCo, hNhuYeuPham, hDichVu;
// Tổng tiền
int totalMoney = 0;
int icount = 0;
// init Function
BOOL OnCreate(HWND hWnd);

// lưu các lựa chọn của người dùng rồi ghi vào file
vector<wstring> vSpend;
vector<wstring> vContent;
vector<wstring> vMoney;

HFONT hFont;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_QLCHITIEU, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QLCHITIEU));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QLCHITIEU));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_QLCHITIEU);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      200, 150, 950, 430, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		OnCreate(hWnd);
	}
	break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
			WCHAR* listSpendBuffer;
			WCHAR* contentBuffer;
			WCHAR* amountofmoneyBuffer;
			int listSpendBufferlen, contentBufferlen, amountofmoneyBufferlen;

            switch (wmId)
            {

			case IDB_BUTTON1:
			{

				// Lấy độ dài chuỗi người dùng nhập
				listSpendBufferlen = GetWindowTextLength(hListSpend);
				contentBufferlen = GetWindowTextLength(hContent);
				amountofmoneyBufferlen = GetWindowTextLength(hAmountofmoney);

				// Chứa nội dung chuỗi người dùng nhập
				listSpendBuffer = new WCHAR[listSpendBufferlen + 1];
				contentBuffer = new WCHAR[contentBufferlen + 1];
				amountofmoneyBuffer = new WCHAR[amountofmoneyBufferlen + 1];

				// Kiểm tra xem người dùng có để trống hay không
				if (0 == contentBufferlen || (0 == amountofmoneyBufferlen))
				{
					MessageBox(0, L"Nhập thiếu nội dung hoặc số tiền", 0, 0);
					break;
				}

				// Tiền hành lấy nội dung rồi thêm vào listbox
				GetWindowText(hListSpend, listSpendBuffer, listSpendBufferlen + 1);
				GetWindowText(hContent, contentBuffer, contentBufferlen + 1);
				GetWindowText(hAmountofmoney, amountofmoneyBuffer, amountofmoneyBufferlen + 1);


				// Thêm một dòng vào listview
				LVITEM lvItem;
				lvItem.mask = LVIF_TEXT | LVCF_FMT;
				lvItem.iItem = icount;
				lvItem.pszText = listSpendBuffer;
				lvItem.piColFmt = LVCFMT_LEFT;
				lvItem.iSubItem = 0;
				ListView_InsertItem(hListView, &lvItem);
				ListView_SetItemText(hListView, icount, 1, contentBuffer);
				ListView_SetItemText(hListView, icount, 2, amountofmoneyBuffer);

				// Thêm vào biến điếm số lượng dòng trong listview (toàn cục)
				icount++;

				// Chuyển đổi tiền
				int money;
				money = _wtoi(amountofmoneyBuffer);

				// Cập nhật tổng tiền
				totalMoney += money;

				WCHAR buffer[100];
				wsprintf(buffer, L"%d", totalMoney);
				SetWindowText(hTotalMoney, buffer);


				

				// Tính tổng tiền lúc sau chia tỉ lệ mỗi loại chi tiêu
				if (wcscmp(listSpendBuffer, L"Ăn uống") == 0)
				{
					moneyOftype[AnUong] += _wtoi(amountofmoneyBuffer);
				}
				else if (wcscmp(listSpendBuffer, L"Di chuyển") == 0)
				{
					moneyOftype[DiChuyen] += _wtoi(amountofmoneyBuffer);
				}
				else if (wcscmp(listSpendBuffer, L"Nhà cữa") == 0)
				{
					moneyOftype[NhaCua] += _wtoi(amountofmoneyBuffer);
				}
				else if (wcscmp(listSpendBuffer, L"Xe cộ") == 0)
				{
					moneyOftype[XeCo] += _wtoi(amountofmoneyBuffer);
				}
				else if (wcscmp(listSpendBuffer, L"Nhu yếu phẩm") == 0)
				{
					moneyOftype[NhuYeuPham] += _wtoi(amountofmoneyBuffer);
				}
				else if (wcscmp(listSpendBuffer, L"Dịch vụ") == 0)
				{
					moneyOftype[DichVu] += _wtoi(amountofmoneyBuffer);
				}

				for (int i = 0; i < 6; i++)
				{
					percentOfType[i] = (moneyOftype[i] * 100) / totalMoney;
				}

				swprintf(buffer, 100, L"%.1f", percentOfType[AnUong]);
				SetWindowText(hAnUong, buffer);

				swprintf(buffer, 100, L"%.1f", percentOfType[DiChuyen]);
				SetWindowText(hDiChuyen, buffer);

				swprintf(buffer, 100, L"%.1f", percentOfType[NhaCua]);
				SetWindowText(hNhaCua, buffer);

				swprintf(buffer, 100, L"%.1f", percentOfType[XeCo]);
				SetWindowText(hXeCo, buffer);

				swprintf(buffer, 100, L"%.1f", percentOfType[NhuYeuPham]);
				SetWindowText(hNhuYeuPham, buffer);

				swprintf(buffer, 100, L"%.1f", percentOfType[DichVu]);
				SetWindowText(hDichVu, buffer);

				// Lưu vào vector chứa nội dung người dùng nhập
				vSpend.push_back(listSpendBuffer);
				vContent.push_back(contentBuffer);
				vMoney.push_back(amountofmoneyBuffer);


				if (!listSpendBuffer)
					delete[] listSpendBuffer;
				if (!contentBuffer)
					delete[] contentBuffer;
				if (!amountofmoneyBuffer)
					delete[] amountofmoneyBuffer;
				draw = 1;
				InvalidateRect(hWnd, NULL, TRUE);
			}
			break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
			int temp = 600;
			HDC hdc = BeginPaint(hWnd, &ps);
			HRGN hrg;


			if (-1 == draw)
			{

				if (totalMoney)
				{
					for (int i = 0; i < 6; i++)
					{
						percentOfType[i] = (moneyOftype[i] / totalMoney) * 100;
						saveShapeSpend[i] = CreateRectRgn(temp, 250 - percentOfType[i]*2, 30 + temp, 250);
						temp += 50;
					}

					// Vẽ trục tọa độ Oy
					MoveToEx(hdc, 560, 30, NULL);
					LineTo(hdc, 560, 250);

					// Vẽ trục tọa độ Ox
					MoveToEx(hdc, 560, 250, NULL);
					LineTo(hdc, 900, 250);

					WCHAR buffer[100];
					// Cặp nhật Ăn uống
					FillRgn(hdc, saveShapeSpend[AnUong], CreateSolidBrush(RGB(255, 0, 0)));
					swprintf(buffer, 100,  L"%.1f", percentOfType[AnUong]);
					SetWindowText(hAnUong, buffer);

					// Cặp nhật  Di chuyển
					FillRgn(hdc, saveShapeSpend[DiChuyen], CreateSolidBrush(RGB(0, 255, 0)));
					swprintf(buffer, 100, L"%.1f", percentOfType[DiChuyen]);
					SetWindowText(hDiChuyen, buffer);

					// Cặp nhật Nhà cữa
					FillRgn(hdc, saveShapeSpend[NhaCua], CreateSolidBrush(RGB(0, 0, 255)));
					swprintf(buffer, 100, L"%.1f", percentOfType[NhaCua]);
					SetWindowText(hNhaCua, buffer);

					// Cặp nhật Xe cộ
					FillRgn(hdc, saveShapeSpend[XeCo], CreateSolidBrush(RGB(255, 255, 0)));
					swprintf(buffer, 100, L"%.1f", percentOfType[XeCo]);
					SetWindowText(hXeCo, buffer);

					// Cặp nhật Nhu yếu phẩm
					FillRgn(hdc, saveShapeSpend[NhuYeuPham], CreateSolidBrush(RGB(255, 0, 255)));
					swprintf(buffer, 100, L"%.1f", percentOfType[NhuYeuPham]);
					SetWindowText(hNhuYeuPham, buffer);

					// Cặp nhật Dịch vụ
					FillRgn(hdc, saveShapeSpend[DichVu], CreateSolidBrush(RGB(0, 255, 255)));
					swprintf(buffer, 100, L"%.1f", percentOfType[DichVu]);
					SetWindowText(hDichVu, buffer);
				}

				// Ăn uống
				hrg = CreateRectRgn(560, 280, 610, 290);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 0, 0)));

				// Di chuyển
				hrg = CreateRectRgn(750, 280, 800, 290);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 255, 0)));

				// Nhà cửa
				hrg = CreateRectRgn(560, 310, 610, 320);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 0, 255)));

				// Xe cộ
				hrg = CreateRectRgn(750, 310, 800, 320);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 255, 0)));

				// Nhu yếu phẩm
				hrg = CreateRectRgn(560, 340, 610, 350);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 0, 255)));

				// Dịch vụ
				hrg = CreateRectRgn(750, 340, 800, 350);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 255, 255)));
				draw = 0;
				//InvalidateRect(hWnd, NULL, TRUE);

			}

			

			if (1 == draw)
			{
				if (totalMoney)
				{
					for (int i = 0; i < 6; i++)
					{
						percentOfType[i] = (moneyOftype[i] * 100) / totalMoney;
						saveShapeSpend[i] = CreateRectRgn(temp, 250 - percentOfType[i]*2, 30 + temp, 250);
						temp += 50;
					}

					// Vẽ trục tọa độ Oy
					MoveToEx(hdc, 560, 30, NULL);
					LineTo(hdc, 560, 250);

					// Vẽ trục tọa độ Ox
					MoveToEx(hdc, 560, 250, NULL);
					LineTo(hdc, 900, 250);

					// Ăn uống
					FillRgn(hdc, saveShapeSpend[AnUong], CreateSolidBrush(RGB(255, 0, 0)));
					

					// Di chuyển
					FillRgn(hdc, saveShapeSpend[DiChuyen], CreateSolidBrush(RGB(0, 255, 0)));
					

					// Nhà cữa
					FillRgn(hdc, saveShapeSpend[NhaCua], CreateSolidBrush(RGB(0, 0, 255)));
					

					// Xe cộ
					FillRgn(hdc, saveShapeSpend[XeCo], CreateSolidBrush(RGB(255, 255, 0)));
					

					// Nhu yếu phẩm
					FillRgn(hdc, saveShapeSpend[NhuYeuPham], CreateSolidBrush(RGB(255, 0, 255)));
					

					// Dịch vụ
					FillRgn(hdc, saveShapeSpend[DichVu], CreateSolidBrush(RGB(0, 255, 255)));
					

				}


				// Ăn uống
				hrg = CreateRectRgn(560, 280, 610, 290);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 0, 0)));

				// Di chuyển
				hrg = CreateRectRgn(750, 280, 800, 290);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 255, 0)));

				// Nhà cửa
				hrg = CreateRectRgn(560, 310, 610, 320);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 0, 255)));

				// Xe cộ
				hrg = CreateRectRgn(750, 310, 800, 320);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 255, 0)));

				// Nhu yếu phẩm
				hrg = CreateRectRgn(560, 340, 610, 350);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(255, 0, 255)));

				// Dịch vụ
				hrg = CreateRectRgn(750, 340, 800, 350);
				FillRgn(hdc, hrg, CreateSolidBrush(RGB(0, 255, 255)));
				draw = 0;
			}
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
	{
		wfstream file;
		file.open("list.txt", ios::out);
		if (!file)
		{
			MessageBox(hWnd, L"Không mở được file!", L"ERROR", MB_OK);
		}
		else
		{
			file.clear();
			locale loc(locale(), new codecvt_utf8<wchar_t>);
			file.imbue(loc);
			for (int i = 0; i < icount; i++)
			{
				file << vSpend[i] << endl;
				file << vContent[i] << endl;
				file << vMoney[i] << endl;
			}
		}
		file.close();

		vSpend.clear();
		vContent.clear();
		vMoney.clear();

		vSpend.~vector();
		vContent.~vector();
		vMoney.~vector();

		icount = 0;

		PostQuitMessage(0);
	}
	break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


BOOL OnCreate(HWND hWnd)
{
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(icc);
	icc.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icc);
	
	draw = -1;

	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	HWND hwnd;
	HFONT hfont = CreateFont(18, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");

	/////////////////////////////    KHUNG THÊM CHI TIÊU      //////////////////////////////////////////////////////////////////////////
	hwnd = CreateWindowEx(0, L"Static", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 10, 10, 510, 80, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Thêm chi tiêu", WS_CHILD | WS_VISIBLE, 30, 2, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hfont, TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE | ES_CENTER, 30, 30, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Nội dung:", WS_CHILD | WS_VISIBLE | ES_CENTER, 150, 30, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Số tiền:", WS_CHILD | WS_VISIBLE | ES_CENTER, 275, 30, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	hContent = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL, 175, 50, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hContent, WM_SETFONT, (WPARAM)hFont, TRUE);

	hAmountofmoney = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL | ES_NUMBER, 305, 50, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hAmountofmoney, WM_SETFONT, (WPARAM)hFont, TRUE);

	hwnd = CreateWindowEx(0, L"Button", L"Thêm", WS_CHILD | WS_VISIBLE | WS_TABSTOP | BS_PUSHBUTTON, 430, 50, 70, 22, hWnd, (HMENU)IDB_BUTTON1, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Vẽ thêm dấu mũi tên cho biểu đồ
	hwnd = CreateWindowEx(0, L"Static", L"^", WS_CHILD | WS_VISIBLE , 558, 30, 20, 5, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Vẽ thêm dấu mũi tên cho biểu đồ
	hwnd = CreateWindowEx(0, L"Static", L">", WS_CHILD | WS_VISIBLE, 900, 243, 70, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Vẽ thêm ký hiệu % cho trục Ox
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 540, 50, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho Ăn uống
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 600, 265, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho Di chuyển
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 600, 295, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho nhà cữa
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 600, 325, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho xe cộ
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 790, 265, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho nhu yếu phẩm
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 790, 295, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm % cho dịch vụ
	hwnd = CreateWindowEx(0, L"Static", L"%", WS_CHILD | WS_VISIBLE, 790, 325, 20, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	// Thêm ký hiệu ăn uống
	hwnd = CreateWindowEx(0, L"Static", L"Ăn uống", WS_CHILD | WS_VISIBLE, 620, 278, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm ký hiệu di chuyển
	hwnd = CreateWindowEx(0, L"Static", L"Di chuyển", WS_CHILD | WS_VISIBLE, 810, 278, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm ký hiệu Nhà cữa
	hwnd = CreateWindowEx(0, L"Static", L"Nhà cữa", WS_CHILD | WS_VISIBLE, 620, 308, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm ký hiệu Xe cộ
	hwnd = CreateWindowEx(0, L"Static", L"Xe cộ", WS_CHILD | WS_VISIBLE, 810, 308, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm ký hiệu nhu yếu phẩm
	hwnd = CreateWindowEx(0, L"Static", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE, 620, 338, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm ký hiệu dịch vụ
	hwnd = CreateWindowEx(0, L"Static", L"Dịch vụ", WS_CHILD | WS_VISIBLE, 810, 338, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	// Thêm loại chi tiêu cho trục Oy
	hwnd = CreateWindowEx(0, L"Static", L"Loại chi tiêu", WS_CHILD | WS_VISIBLE, 870, 260, 100, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);


	// Tạo danh sách các loại chi tiêu bằng combobox
	hListSpend = CreateWindowEx(0, L"COMBOBOX", L"Choice", WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWN | CBS_HASSTRINGS | CBS_SIMPLE | CBS_AUTOHSCROLL, 50, 50, 100, 100, hWnd, NULL, hInst, NULL);
	SendMessage(hListSpend, WM_SETFONT, (WPARAM)hFont, TRUE);


	// Các loại chi tiêu
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Ăn uống");
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Di chuyển");
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Nhà cữa");
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Xe cộ");
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Nhu yếu phẩm");
	SendMessage(hListSpend, CB_ADDSTRING, 0, (LPARAM)L"Dịch vụ");
	
	// Hiện cái đầu tiên
	SendMessage(hListSpend, CB_SETCURSEL, 0, 0);

	/////////////////////////////    KHUNG THÊM CHI TIÊU  //////////////////////////////////////////////////////////////////////////

	/////////////////////////////    KHUNG DANH SÁCH CHI TIÊU      //////////////////////////////////////////////////////////////////////////


	hwnd = CreateWindowEx(0, L"Static", L"", WS_CHILD | WS_VISIBLE | WS_BORDER, 10, 110, 510, 240, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hFont, TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | ES_CENTER, 30, 100, 130, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hfont, TRUE);

	hListView = CreateWindow(WC_LISTVIEW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_EDITLABELS, 30, 130, 470, 180, hWnd, NULL, hInst, NULL);
	SendMessage(hListView, WM_SETFONT, (WPARAM)hFont, TRUE);

	hwnd = CreateWindowEx(0, L"Static", L"Tổng cộng", WS_CHILD | WS_VISIBLE | ES_LEFT, 270, 320, 100, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, (WPARAM)hfont, TRUE);

	hTotalMoney = CreateWindowEx(0, L"Edit", L"", WS_CHILD | WS_VISIBLE | ES_CENTER | ES_READONLY | WS_BORDER, 350, 320, 150, 20, hWnd, NULL, hInst, NULL);
	SendMessage(hTotalMoney, WM_SETFONT, (WPARAM)hFont, TRUE);

	hAnUong = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 560, 265, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hDiChuyen = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 750, 265, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hNhaCua = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 560, 295, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hXeCo = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 750, 295, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hNhuYeuPham = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 560, 325, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

	hDichVu = CreateWindowEx(0, L"Edit", L"0.0", WS_CHILD | WS_VISIBLE | ES_READONLY, 750, 325, 40, 15, hWnd, NULL, hInst, NULL);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);


	// Tạo các cột của list view để hiển thị danh sách các chi tiêu
	LVCOLUMN lvCol1, lvCol2, lvCol3;

	// Cột 1 Loại thu phí
	lvCol1.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol1.fmt = LVCFMT_LEFT;
	lvCol1.pszText = L"Loại chi tiêu";
	lvCol1.cx = 180;
	ListView_InsertColumn(hListView, 0, &lvCol1);

	// Cột 2 Nội dung
	lvCol2.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol2.fmt = LVCFMT_LEFT;
	lvCol2.pszText = L"Nội dung";
	lvCol2.cx = 180;
	ListView_InsertColumn(hListView, 1, &lvCol2);

	// Cột 3 số tiền
	lvCol3.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol3.fmt = LVCFMT_LEFT;
	lvCol3.pszText = L"Số tiền";
	lvCol3.cx = 130;
	ListView_InsertColumn(hListView, 2, &lvCol3);

	/////////////////////////////    KHUNG DANH SÁCH CHI TIÊU      //////////////////////////////////////////////////////////////////////////

	/////////////////////////////    LOAD FILE DANH SÁCH CHI TIÊU  //////////////////////////////////////////////////////////////////////////

	wfstream ifile;
	ifile.open("list.txt", ios::in);
	
	if (!ifile)
	{
		ifile.open("list.txt", ios::out);
		ifile.clear();
		ifile.close();
	}

	else
	{
		// Đọc unicode
		locale mylocale(locale(), new codecvt_utf8<wchar_t>);

		WCHAR read1[100];
		WCHAR read2[100];
		WCHAR read3[100];
		WCHAR buffer[100];
		int type; // Loại chi tiêu
		WCHAR content[100]; // Nội dung
		int money; // Số tiền

		ifile.imbue(mylocale);
		while (!ifile.eof())
		{
			// Kiểm tra tránh chuỗi rỗng
			// Vì lần đầu ghi file bị 3 dòng trắng
			ifile.getline(read1, 100);
			if (wcscmp(read1, L"") == 1)
			{
				
				ifile.getline(read2, 100);
				ifile.getline(read3, 100);


				// Thêm item vào listview
				LVITEM lvR;
				lvR.mask = LVIF_TEXT | LVCF_FMT;
				lvR.iItem = icount;
				lvR.pszText = read1;
				lvR.piColFmt = LVCFMT_LEFT;
				lvR.iSubItem = 0;
				ListView_InsertItem(hListView, &lvR);
				ListView_SetItemText(hListView, icount, 1, read2);
				ListView_SetItemText(hListView, icount, 2, read3);

				// Thêm vào biến điếm số lượng dòng trong listview (toàn cục)
				icount++;

				// Convert wchar sang giá trị tương ứng
				money = _wtoi(read3);

				// Thêm vào tổng tiền 
				totalMoney += money;
				InvalidateRect(hWnd, NULL, TRUE);

				// Tính tổng tiền lúc sau chia tỉ lệ mỗi loại chi tiêu
				if (wcscmp(read1, L"Ăn uống") == 0)
				{
					moneyOftype[AnUong] += _wtoi(read3);
				}
				else if (wcscmp(read1, L"Di chuyển") == 0)
				{
					moneyOftype[DiChuyen] += _wtoi(read3);
				}
				else if (wcscmp(read1, L"Nhà cữa") == 0)
				{
					moneyOftype[NhaCua] += _wtoi(read3);
				}
				else if (wcscmp(read1, L"Xe cộ") == 0)
				{
					moneyOftype[XeCo] += _wtoi(read3);
				}
				else if (wcscmp(read1, L"Nhu yếu phẩm") == 0)
				{
					moneyOftype[NhuYeuPham] += _wtoi(read3);
				}
				else if (wcscmp(read1, L"Dịch vụ") == 0)
				{
					moneyOftype[DichVu] += _wtoi(read3);
				}

				// Lưu vào khi destroy thì chép vào file
				vSpend.push_back(read1);
				vContent.push_back(read2);
				vMoney.push_back(read3);

			}
			else if (wcscmp(read1, L"") == 0)
			{
				break;
			}
		}
		/////////////////////////////    LOAD FILE DANH SÁCH CHI TIÊU  //////////////////////////////////////////////////////////////////////////
		
		// Cho hiện tổng tiền
		wsprintf(buffer, L"%d", totalMoney);
		SetWindowText(hTotalMoney, buffer);
		
	}
	ifile.close();

	return TRUE;
}
